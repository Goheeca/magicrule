@echo off
setlocal

set ARTISTIC_STYLE_OPTIONS=.astylerc

astyle --suffix=none --recursive src/*.java
astyle --suffix=none --recursive test/*.java

endlocal