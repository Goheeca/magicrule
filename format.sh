#!/bin/bash

ARTISTIC_STYLE_OPTIONS=.astylerc astyle --suffix=none --recursive "src/*.java"
ARTISTIC_STYLE_OPTIONS=.astylerc astyle --suffix=none --recursive "test/*.java"
