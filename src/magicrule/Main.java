package magicrule;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import magicrule.reader.Reader;
import magicrule.writer.Writer;
/**
 * The Class Main is handling the input and output.
 */
public class Main {

  /**
   * Traverses the input and generates the magic form and records it to the output.
   *
   * @param args the first argument is the input directory and the second argument is the output directory
   */
  public static void main(String[] args) {
	if(args.length != 3) {
		System.out.print(help());
		System.exit(0);
	}
	if(!args[0].matches("all|magic")) {
		System.err.println("Bad type of output");
		System.err.print(help());
		System.exit(1);
	}
	Writer.allOutput = args[0].equals("all");
    try {
      Path outputPath = FileSystems.getDefault().getPath(args[2]);
      Path inputPath = FileSystems.getDefault().getPath(args[1]);
      System.out.println("INPUT: " + inputPath);
      System.out.println("OUTPUT: " + outputPath);
      Stream<Path> input = StreamSupport.stream(Files.newDirectoryStream(inputPath).spliterator(), false);
      input.parallel().filter(Files::isDirectory).forEach(ruleDirectory -> {
        try {
          //System.out.println(ruleDirectory);
          Stream<Path> ruleFiles = StreamSupport.stream(Files.newDirectoryStream(ruleDirectory).spliterator(), false);
          ruleFiles.parallel().filter(Files::isRegularFile).forEach(ruleFile -> {
            System.out.println(ruleFile);
            Record record = Reader.readRecord(ruleFile.toFile());
            record.generateMagicForm();

            Path part = ruleFile.subpath(ruleFile.getNameCount() - 2, ruleFile.getNameCount());
            Path output = outputPath.resolve(part);

            try {
              Files.createDirectories(output.getParent());
              Files.deleteIfExists(output);
              Files.createFile(output);

              Writer.writeRecord(output.toFile(), record);
            } catch (Exception e) {
              e.printStackTrace();
            }
          });
        } catch (Exception e) {
          e.printStackTrace();
        }
      });

      //Record record = Reader.readRecord(new File(args[0]));
      //System.out.println(record.parts);
      //record.generateMagicForm();
      //Writer.writeRecord(new File("output.txt"), record);
    } catch (RuntimeException | IOException e) {
      System.err.println("Error: " + e.getMessage() + ", inner info:");
      System.err.println(e.getCause());
    }
  }
  
  /**
   * Informs about usage.
   *
   * @return help message
   */
  public static String help() {
	  return "Usage:\n"
			  + "#1 argument -- type of output: [all|magic]\n"
			  + "#2 argument -- input directory\n"
			  + "#3 argument -- output directory\n";
  } 
}
