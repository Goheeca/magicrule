package magicrule;

/**
 * The Class Pair enables a more functional programming style.
 *
 * @param <F> the first type
 * @param <S> the second type
 */
public class Pair<F, S> {

  /** The first value. */
  public final F first;

  /** The second value. */
  public final S second;

  /**
   * Instantiates a new pair.
   *
   * @param first the first value
   * @param second the second value
   */
  public Pair(F first, S second) {
    this.first = first;
    this.second = second;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return first.hashCode() ^ second.hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;

    if (obj == null)
      return false;

    if (getClass() != obj.getClass())
      return false;

    @SuppressWarnings("unchecked")
    Pair<F, S> other = (Pair<F, S>) obj;
    return this.first.equals(other.first) && this.second.equals(other.second);
  }
}
