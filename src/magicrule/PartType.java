package magicrule;

import java.util.regex.Pattern;

/**
 * The Enum PartType is used for distinguishing the sections.
 */
public enum PartType {

  /** The original rule. */
  ORIGINAL_RULE("Original rule:"),
  /** The modified rule. */
  MODIFIED_RULE("Modified rule: \\([bf]+\\)"),
  /** The sideways information passing graph. */
  SIP("SIP:"),
  /** The final sideways information passing graph. */
  FSIP("FSIP:"),
  /** The adorned rule. */
  ADORNED_RULE("Adorned rule:"),
  /** The magic rules. */
  MAGIC_RULES("Magic rules:");

  /** The pattern matching the header of a particular section. */
  public final Pattern pattern;

  /**
   * Instantiates a new part type.
   *
   * @param regex the regex of header matcher
   */
  private PartType(String regex) {
    this.pattern = Pattern.compile(regex);
  }
}
