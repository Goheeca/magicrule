package magicrule;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import magicrule.graph.PassingInformation;
import magicrule.graph.SIPGraph;
import magicrule.logic.Atom;
import magicrule.logic.NormalVariable;
import magicrule.logic.PositionalAtom;
import magicrule.logic.Rule;
import magicrule.logic.Symbol;
import magicrule.parser.Definitions;
import magicrule.parser.Lexeme;
import magicrule.parser.Lexer;
import magicrule.parser.Parser;
import magicrule.parser.Token;
import magicrule.reader.Partitioner.Part;
import magicrule.writer.Writer;

/**
 * The Class Record holds an unit for transformation (it corresponds with one file).
 */
public class Record {

  /** The Constant HEADER_REGEX is a regex matching the suffix of a header symbol. */
  public static final String HEADER_REGEX = ".*?_h";

  /** The Constant POSITION_DELIM_REGEX is a regex matching the delimiter of a plain symbol and an ordinal position. */
  public static final String POSITION_DELIM_REGEX = "\\.";

  /** The Constant POSITION_DELIM is the delimiter of a plain symbol and an ordinal position. */
  public static final String POSITION_DELIM = ".";

  /** The Constant START_COUNT is the initial value for the first occurrence of an atom. */
  public static final Integer START_COUNT = 1;

  /** The sections of the record. */
  public final List<Part> parts;

  /** The sideways information passing graph. */
  private final SIPGraph SIP;

  /** The adorned rule. */
  private final Rule adornedRule;

  /** The helper structure for conversion between atom occurrences in the rule and the SIP graph. */
  private Map<Symbol, List<PositionalAtom>> positioned;

  /**
   * Instantiates a new record.
   *
   * @param parts the sections
   * @param SIP the SIP graph
   * @param adornedRule the adorned rule
   */
  public Record(List<Part> parts, String SIP, String adornedRule) {
    this.parts = parts;

    // Parsing of an adorned rule
    List<Lexeme<Definitions.Logic>> lexemes = Lexer.lexicalize(Definitions.logicProgramLexers, new StringBuilder(adornedRule));
    List<Token<Definitions.Logic>> tokens = Token.tokenize(lexemes);
    this.adornedRule = Parser.parse(new Definitions.RuleConstructor(), Definitions.RuleParserState.HeaderSymbol, Definitions.ruleParserTransitions, tokens);
    //System.out.println(this.adornedRule);

    // Parsing of a sideways information passing graph
    List<Lexeme<Definitions.Graph>> SIPLexemes = Lexer.lexicalize(Definitions.graphLexers, new StringBuilder(SIP));
    List<Token<Definitions.Graph>> SIPTokens = Token.tokenize(SIPLexemes);
    this.SIP = Parser.parse(new Definitions.GraphConstructor(), Definitions.GraphParserState.PathOpeningBrace, Definitions.graphParserTransitions, SIPTokens);
    //System.out.println(this.SIP);
    //System.out.println(this.adornedRule.body.get(this.adornedRule.body.size() - 2).magicForm());
  }

  /**
   * Converts from the SIP graph to the occurrence in the rule.
   *
   * @param symbol the vertex symbol
   * @return the occurrence of an atom
   */
  private PositionalAtom getAdornedBySIP(Symbol symbol) {
    // Testing for a special header predicate
    if (symbol.identifier.matches(HEADER_REGEX))
      return new PositionalAtom(adornedRule.header);

    // Replacing the symbol with the occurrence of predicate
    String[] parts = symbol.identifier.split(POSITION_DELIM_REGEX);
    return positioned.get(new Symbol(parts[0])).get(Integer.parseInt(parts[1]) - START_COUNT);
  }

  /**
   * Converts from the occurrence in the rule to the SIP graph.
   *
   * @param adorned the occurrence of an atom
   * @return the vertex symbol
   */
  private Symbol getSIPByAdorned(PositionalAtom adorned) {
    return new Symbol(adorned.plainSymbol().identifier + POSITION_DELIM + adorned.position);
  }

  /**
   * Generates magic rules for the given rule.
   */
  public void generateMagicForm() {
    // Enumerating of the occurrences
    List<PositionalAtom> bodyPositioned = adornedRule.body.stream()
                                          .map(predicate -> new PositionalAtom(predicate)).collect(Collectors.toList());
    // Creating of a map for the conversion from a SIP graph symbol to an occurrence of predicate in the rule
    positioned = bodyPositioned.stream().collect(Collectors.groupingBy(PositionalAtom::plainSymbol, Collectors.toList()));

    // Setting up the ordinal positions for the conversion from an occurrence of predicate to a SIP graph symbol
    positioned.forEach((symbol, positionedAtoms) ->
                       Util.zip(Stream.generate(Util.counter(START_COUNT)), positionedAtoms.stream(), Pair<Integer, PositionalAtom>::new)
                       .forEach(pair -> pair.second.position = pair.first));

    // Collecting of magic rules for every adorned occurrence of predicate
    List<Rule> magicRules = bodyPositioned.stream().filter(predicate -> predicate.isAdorned()).map(adorned -> magicRule(adorned)).collect(Collectors.toList());
    // Writing out the magic rules for the output
    Part magicPart = new Part("Magic rules:", magicRules.stream().map(Rule::toString).collect(Collectors.joining(Writer.newLine + Writer.newLine)));
    // Used by Writer for a potential empty result
    magicPart.type = Optional.of(PartType.MAGIC_RULES);
    parts.add(magicPart);
  }

  /**
   * Generates a magic rule for the intensional predicate.
   *
   * @param adorned the intensional predicate.
   * @return the magic rule
   */
  private Rule magicRule(PositionalAtom adorned) {
    // Prepare parts of rule's body; with unique occurrences and preserving order
    Set<Atom> specialHeader = new LinkedHashSet<>();
    Set<Atom> magic = new LinkedHashSet<>();
    Set<Atom> original = new LinkedHashSet<>();

    //System.out.println(SIP.withTail(getSIPByAdorned(adorned)));

    // For every edge which ends in this occurrence of predicate
    SIP.withTail(getSIPByAdorned(adorned)).forEach(edge -> {
      //System.out.println("(" + getAdornedBySIP(SIP.head(edge)) + ", " + getAdornedBySIP(SIP.tail(edge)) + ")");
      // Process the edge
      Pair<Set<Atom>, Pair<Set<Atom>, Set<Atom>>> partialResult = resolveEdge(edge);
      // Append the partial result to the parts of rule's body
      specialHeader.addAll(partialResult.first);
      magic.addAll(partialResult.second.first);
      original.addAll(partialResult.second.second);
    });

    // Return a new rule with concatenated parts of body
    return new Rule(adorned.magicForm(),
                    Stream.concat(specialHeader.stream(), Stream.concat(magic.stream(), original.stream())).collect(Collectors.toList()));
  }

  /**
   * Resolves only one edge in the SIP graph.
   *
   * @param edge the edge
   * @return the triplet of parts of constructed body of a magic rule
   */
  private Pair<Set<Atom>, Pair<Set<Atom>, Set<Atom>>> resolveEdge(PassingInformation edge) {
    // Prepare parts of rule's body; with unique occurrences and preserving order
    Set<Atom> specialHeader = new LinkedHashSet<>();
    Set<Atom> magic = new LinkedHashSet<>();
    Set<Atom> original = new LinkedHashSet<>();

    // An occurrence of predicate from which comes the information
    PositionalAtom informationSource = getAdornedBySIP(SIP.head(edge));

    // Case of the special header predicate
    if (informationSource.predicateSymbol.equals(adornedRule.header.predicateSymbol)) {
      specialHeader.add(adornedRule.header.magicForm());
    } else {
      // Add original form
      original.add(informationSource);

      // The occurrence of predicate is an intensional predicate
      if (informationSource.isAdorned()) {
        // Check whether exists a bound argument in the source predicate
        if (edge.variables.stream().anyMatch(argument ->
                                             informationSource.arguments.stream()
                                             .filter(term -> term instanceof NormalVariable)
                                             .anyMatch(term -> argument.equals((NormalVariable)term)))) {
          // Add magic form
          magic.add(informationSource.magicForm());
        }

        // The occurrence of predicate is an extensional predicate
      } else {
        // Recursively process the preceding vertex
        SIP.withTail(getSIPByAdorned(informationSource)).forEach(edgeNext -> {
          // Process the edge
          Pair<Set<Atom>, Pair<Set<Atom>, Set<Atom>>> partialResult = resolveEdge(edgeNext);
          // Append the partial result
          specialHeader.addAll(partialResult.first);
          magic.addAll(partialResult.second.first);
          original.addAll(partialResult.second.second);
        });
      }
    }

    // Return partial result in parts
    return new Pair<>(specialHeader, new Pair<>(magic, original));
  }
}
