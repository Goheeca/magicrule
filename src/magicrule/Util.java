package magicrule;

import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * The Class Util aggregates a helper functionality for functional style of programming.
 */
public class Util {

  /**
   * Zips two streams into the one stream.
   *
   * @param <A> the first type
   * @param <B> the second type
   * @param <C> the resulting type
   * @param a the first stream
   * @param b the second stream
   * @param zipper the zipper which ensures the combining of two input types into the resulting type
   * @return the resulting stream
   */
  // http://stackoverflow.com/a/23529010/1721219
  public static<A, B, C> Stream<C> zip(Stream<? extends A> a,
                                       Stream<? extends B> b,
                                       BiFunction<? super A, ? super B, ? extends C> zipper) {
    Objects.requireNonNull(zipper);
    @SuppressWarnings("unchecked")
    Spliterator<A> aSpliterator = (Spliterator<A>) Objects.requireNonNull(a).spliterator();
    @SuppressWarnings("unchecked")
    Spliterator<B> bSpliterator = (Spliterator<B>) Objects.requireNonNull(b).spliterator();

    // Zipping looses DISTINCT and SORTED characteristics
    int both = aSpliterator.characteristics() & bSpliterator.characteristics() &
               ~(Spliterator.DISTINCT | Spliterator.SORTED);
    int characteristics = both;

    long zipSize = ((characteristics & Spliterator.SIZED) != 0)
                   ? Math.min(aSpliterator.getExactSizeIfKnown(), bSpliterator.getExactSizeIfKnown())
                   : -1;

    Iterator<A> aIterator = Spliterators.iterator(aSpliterator);
    Iterator<B> bIterator = Spliterators.iterator(bSpliterator);
    Iterator<C> cIterator = new Iterator<C>() {
      @Override
      public boolean hasNext() {
        return aIterator.hasNext() && bIterator.hasNext();
      }

      @Override
      public C next() {
        return zipper.apply(aIterator.next(), bIterator.next());
      }
    };

    Spliterator<C> split = Spliterators.spliterator(cIterator, zipSize, characteristics);
    return (a.isParallel() || b.isParallel())
           ? StreamSupport.stream(split, true)
           : StreamSupport.stream(split, false);
  }

  /**
   * The Class Counter implements Supplier of ascending numbers.
   */
  private static class Counter implements Supplier<Integer> {

    /** The current count. */
    private int count;

    /**
     * Instantiates a new counter.
     *
     * @param init the init value
     */
    public Counter(int init) {
      count = init;
    }

    /* (non-Javadoc)
     * @see java.util.function.Supplier#get()
     */
    @Override
    public Integer get() {
      return count++;
    }
  }

  /**
   * Returns supplier of ascending numbers.
   *
   * @param init the init value
   * @return the supplier
   */
  public static Supplier<Integer> counter(int init) {
    return new Counter(init);
  }
}
