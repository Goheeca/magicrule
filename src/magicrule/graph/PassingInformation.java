package magicrule.graph;

import java.util.Set;

import magicrule.Pair;
import magicrule.logic.NormalVariable;
import magicrule.logic.Symbol;

/**
 * The Class PassingInformation represents the edges in SIP graphs.
 */
public class PassingInformation {

  /** The variables are the weight of the edge (information passing variables). */
  public final Set<NormalVariable> variables;

  /** The endpoints holds the head and tail of the edge. */
  public final Pair<Symbol, Symbol> endpoints;

  /**
   * Instantiates an edge.
   *
   * @param variables the weight (information passing variables)
   * @param head the head of an edge
   * @param tail the tail of an edge
   */
  public PassingInformation(Set<NormalVariable> variables, Symbol head, Symbol tail) {
    this.variables = variables;
    endpoints = new Pair<>(head, tail);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "(" + endpoints.first + ", " + endpoints.second + ") -> " + variables.toString();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return variables.hashCode() ^ endpoints.hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;

    if (obj == null)
      return false;

    if (obj instanceof PassingInformation) {
      PassingInformation other = (PassingInformation) obj;
      return this.endpoints.equals(other.endpoints) && this.variables.equals(other.variables);
    }

    return false;
  }
}
