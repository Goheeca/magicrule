package magicrule.graph;

import java.util.HashSet;
import java.util.Set;

import magicrule.logic.Symbol;

/**
 * The Class Predicate represents vertices in SIP graphs.
 */
public class Predicate extends Symbol {

  /** The head of aggregates edges which has this vertex as the head. */
  public final Set<PassingInformation> headOf = new HashSet<>();

  /** The tail of aggregates edges which has this vertex as the tail. */
  public final Set<PassingInformation> tailOf = new HashSet<>();

  /**
   * Instantiates a new vertex.
   *
   * @param symbol the vertex symbol
   */
  public Predicate(Symbol symbol) {
    super(symbol.identifier);
  }
}
