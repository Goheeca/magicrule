package magicrule.graph;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import magicrule.Pair;
import magicrule.logic.NormalVariable;
import magicrule.logic.Symbol;

/**
 * The Class SIPGraph represents a graph of sideways information passing relations.
 */
public class SIPGraph {

  /** The predicates is mapping of symbols to graph vertices. */
  private final Map<Symbol, Predicate> predicates = new HashMap<>();

  /** The passingInformation holds all the edges of a graph. */
  private final Map<Pair<Symbol, Symbol>, PassingInformation> passingInformation = new HashMap<>();

  /**
   * Returns edges which contains the given vertex as a head end.
   *
   * @param symbol symbol representing a vertex
   * @return a set of edges
   */
  public Set<PassingInformation> withHead(Symbol symbol) {
    return predicates.get(symbol).headOf;
  }

  /**
   * Returns edges which contains the given vertex as a tail end.
   *
   * @param symbol symbol representing a vertex
   * @return a set of edges
   */
  public Set<PassingInformation> withTail(Symbol symbol) {
    return predicates.get(symbol).tailOf;
  }

  /**
   * Returns the symbol in the head of an edge.
   *
   * @param passingInformation edge
   * @return vertex symbol
   */
  public Symbol head(PassingInformation passingInformation) {
    return passingInformation.endpoints.first;
  }

  /**
   * Returns the symbol in the head of an edge.
   *
   * @param passingInformation edge
   * @return vertex symbol
   */
  public Symbol tail(PassingInformation passingInformation) {
    return passingInformation.endpoints.second;
  }

  /**
   * Returns an edge between two vertices.
   *
   * @param from head vertex symbol
   * @param to tail vertex symbol
   * @return edge
   */
  public PassingInformation getPassingInformation(Symbol from, Symbol to) {
    return passingInformation.get(new Pair<>(from, to));
  }

  /**
   * Adds an edge to the graph, it's a building function.
   *
   * @param f the head of an edge
   * @param t the tail of an edge
   * @param variables the weight of an edge (variables passing information)
   */
  public void addPassingInformation(Symbol f, Symbol t, Set<NormalVariable> variables) {
    if (!predicates.containsKey(f))
      predicates.put(f, new Predicate(f));

    if (!predicates.containsKey(t))
      predicates.put(t, new Predicate(t));

    PassingInformation pI = new PassingInformation(variables, f, t);

    withHead(f).add(pI);
    withTail(t).add(pI);
    passingInformation.put(new Pair<>(f, t), pI);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SIPGraph<vertices: " + predicates + ", edges: " + passingInformation + ">";
  }
}
