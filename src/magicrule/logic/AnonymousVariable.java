package magicrule.logic;

/**
 * The Class AnonymousVariable is a special case of Variable.
 */
public class AnonymousVariable extends Variable {
  public boolean distinct;

  public AnonymousVariable(boolean distinct) {
    this.distinct = distinct;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return distinct ? super.hashCode() : getClass().hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this)
      return true;

    if (obj == null)
      return false;

    if (obj instanceof AnonymousVariable) {
      if (!distinct)
        return true;
    }

    return false;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "_";
  }
}
