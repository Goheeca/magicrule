package magicrule.logic;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import magicrule.Util;

/**
 * The Class Atom.
 */
public class Atom {

  /** The adornment is pattern for suffix denoting an adornment. */
  public final Pattern adornment = Pattern.compile("_([bf]+)$");

  /** The plain is pattern for stripping an adornment. */
  public final Pattern plain = Pattern.compile("(.*?)_[bf]+$");

  /** The predicate symbol. */
  public final Symbol predicateSymbol;

  /** The arguments (list of terms). */
  public final List<Term> arguments;

  /** The flag for negative literal. */
  public final boolean negated;

  /**
   * Instantiates a new atom.
   *
   * @param predicateSymbol the predicate symbol (in our case uniquely representing the atom alone)
   * @param arguments the arguments (the arity is fixed to the predicate symbol)
   * @param negated marks the atom as negative literal
   */
  public Atom(Symbol predicateSymbol, List<Term> arguments, boolean negated) {
    this.predicateSymbol = predicateSymbol;
    this.arguments = arguments;
    this.negated = negated;
  }

  /**
   * Returns a predicate symbol without the adornment.
   *
   * @return the symbol
   */
  public Symbol plainSymbol() {
    if (!isAdorned())
      return predicateSymbol;

    Matcher m = plain.matcher(predicateSymbol.identifier);
    m.find();
    return new Symbol(m.group(1));
  }

  /**
   * Checks if the predicate symbol is adorned.
   *
   * @return true, if is adorned
   */
  public boolean isAdorned() {
    return adornment.matcher(predicateSymbol.identifier).find();
  }

  /**
   * Returns a magic form of the atom, adding an particular prefix and stripping unbounded arguments.
   *
   * @return the atom
   */
  public Atom magicForm() {
    Matcher m = adornment.matcher(predicateSymbol.identifier);

    if (!m.find())
      throw new RuntimeException("The atom is not in an adorned form.");

    List<Boolean> bounded = m.group(1).chars().mapToObj(character -> character == 'b').collect(Collectors.toList());
    List<Term> boundedArguments = Util.zip(arguments.stream(), bounded.stream(), (argument, bound) -> bound ? argument : null).filter(argument -> argument != null).collect(Collectors.toList());
    return new Atom(new Symbol("m_" + predicateSymbol.identifier), boundedArguments, negated);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return predicateSymbol.hashCode() ^ arguments.hashCode() ^ Boolean.hashCode(negated);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this)
      return true;

    if (obj == null)
      return false;

    if (obj instanceof Atom) {
      Atom other = (Atom) obj;
      return predicateSymbol.equals(other.predicateSymbol)
             && negated == other.negated
             && arguments.equals(other.arguments);
    }

    return false;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    String toString = arguments.stream().map(argument -> argument.toString()).collect(Collectors.joining(", ", "(", ")"));

    if (negated) {
      return "not (" + predicateSymbol + toString + ")";
    } else {
      return predicateSymbol + toString;
    }
  }
}
