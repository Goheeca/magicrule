package magicrule.logic;


/**
 * The Class Constant is a special case of Term.
 */
public class Constant extends Term {

  /** The number. */
  private final Double number;

  /** The string. */
  private final String string;

  /**
   * Instantiates a new constant as a numerical constant.
   *
   * @param number the number value of a constant
   */
  public Constant(Double number) {
    this.number = number;
    string = null;
  }

  /**
   * Instantiates a new constant as a string constant.
   *
   * @param string the string value of a constant
   */
  public Constant(String string) {
    this.string = string;
    number = null;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return number != null ? number.hashCode() : string.hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this)
      return true;

    if (obj == null)
      return false;

    if (obj instanceof Constant) {
      Constant other = (Constant) obj;
      return (number != null && number.equals(other.number))
             || (string != null && string.equals(other.string));
    }

    return false;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    if (number != null) return number.toString();
    else return "'" + string + "'";
  }
}
