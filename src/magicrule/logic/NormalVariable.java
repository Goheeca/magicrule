package magicrule.logic;

/**
 * The Class NormalVariable is a special case of Variable.
 */
public class NormalVariable extends Variable {

  /** The symbol is representing variable's identifier. */
  public final Symbol symbol;

  /**
   * Instantiates a new normal variable.
   *
   * @param identifier the identifier of the variable
   */
  public NormalVariable(String identifier) {
    symbol = new Symbol(identifier);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return symbol.hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;

    if (obj == null)
      return false;

    if (getClass() != obj.getClass())
      return false;

    NormalVariable other = (NormalVariable) obj;
    return this.symbol.equals(other.symbol);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return symbol.toString();
  }
}
