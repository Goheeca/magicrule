package magicrule.logic;

/**
 * The Class PositionalAtom extends normal Atom with Position which is used for conversions between logic programs and SIP graphs.
 */
public class PositionalAtom extends Atom {

  /** The position indicating an occurrence in a logic program. */
  public int position;

  /**
   * Instantiates a new positional atom without a position.
   *
   * @param atom the original atom
   */
  public PositionalAtom(Atom atom) {
    super(atom.predicateSymbol, atom.arguments, atom.negated);
  }

  /*@Override
  public String toString() {
    return "Atom<position: " + position + ", value: " + super.toString() + ">";
  }*/
}
