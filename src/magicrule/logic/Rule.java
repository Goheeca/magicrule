package magicrule.logic;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The Class Rule represents units of logic program. The basic units that are being transformed.
 */
public class Rule {

  /** The header atom of an rule. */
  public final Atom header;

  /** The body consisting of list of atoms. */
  public final List<Atom> body;

  /**
   * Instantiates a new rule.
   *
   * @param header the header atom
   * @param body the body (list of atoms)
   */
  public Rule(Atom header, List<Atom> body) {
    this.header = header;
    this.body = body;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return header.hashCode() ^ body.hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this)
      return true;

    if (obj == null)
      return false;

    if (obj instanceof Rule) {
      Rule other = (Rule) obj;
      return header.equals(other.header) && body.equals(other.body);
    }

    return false;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    String sep = System.getProperty("line.separator");
    String body = this.body.stream().map(atom -> atom.toString()).collect(Collectors.joining("," + sep + "  ", sep + "  ", ""));
    return header + " :-" + body + ".";
  }
}
