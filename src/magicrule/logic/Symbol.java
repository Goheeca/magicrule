package magicrule.logic;

/**
 * The Class Symbol is essentially an unique identifier textually represented as a unique string.
 */
public class Symbol {

  /** The textual representation. */
  public final String identifier;

  /**
   * Instantiates a new symbol.
   *
   * @param identifier the identifier
   */
  public Symbol(String identifier) {
    this.identifier = identifier;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return identifier.hashCode();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;

    if (obj == null)
      return false;

    if (getClass() != obj.getClass())
      return false;

    Symbol other = (Symbol) obj;
    return this.identifier.equals(other.identifier);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return identifier;
  }
}
