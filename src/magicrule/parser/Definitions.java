package magicrule.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

import magicrule.Pair;
import magicrule.graph.SIPGraph;
import magicrule.logic.AnonymousVariable;
import magicrule.logic.Atom;
import magicrule.logic.Constant;
import magicrule.logic.NormalVariable;
import magicrule.logic.Rule;
import magicrule.logic.Symbol;
import magicrule.logic.Term;

/**
 * The Class Definitions aggregates defined input languages and theirs parsers.
 */
public class Definitions {

  /**
   * The Interface ElementConstructor for constructing an output during parsing of the input.
   *
   * @param <T> the abstract representation of the input
   */
  public static interface ElementConstructor<T> {

    /**
     * Constructs the output.
     *
     * @return the abstract representation being parsed
     */
    public T construct();
  }

  /**
   * The Enum Logic is family of types for lexemes and tokens of logic programs.
   */
  public static enum Logic {

    /** The Capital symbol. */
    CapitalSymbol,
    /** The Lower symbol. */
    LowerSymbol,
    /** The Not keyword. */
    NotKeyword,
    /** The Anonymous symbol. */
    AnonymousSymbol,
    /** The Number. */
    Number,
    /** The String. */
    String,
    /** The Comma. */
    Comma,

    /** The Full stop. */
    FullStop,
    /** The Left parenthesis. */
    LeftParenthesis,
    /** The Right parenthesis. */
    RightParenthesis,
    /** The Left implication. */
    LeftImplication,
    /** The Query. */
    Query
  };

  /** The lexers for recognizing lexemes in logic programs. */
  public static Set<Lexer<Logic>> logicProgramLexers = new HashSet<>();
  static {
    Collections.addAll(
      logicProgramLexers,
      new Lexer<>(null, "\\p{javaWhitespace}+"), // Lexer for fake lexemes
      new Lexer<>(Definitions.Logic.CapitalSymbol, "[A-Z][a-zA-Z0-9_]*"),
      new Lexer<>(Definitions.Logic.LowerSymbol, "(?!not)[a-z][a-zA-Z0-9_\\.]*"),
      new Lexer<>(Definitions.Logic.NotKeyword, "not"),
      new Lexer<>(Definitions.Logic.AnonymousSymbol, "_"),
      new Lexer<>(Definitions.Logic.Number, "-?(?:0|[1-9][0-9]*)\\.[0-9]+|-?[1-9][0-9]*|0"),
      new Lexer<>(Definitions.Logic.String, "'[^']*'"),
      new Lexer<>(Definitions.Logic.Comma, ","),
      new Lexer<>(Definitions.Logic.FullStop, "\\."),
      new Lexer<>(Definitions.Logic.LeftImplication, ":-"),
      new Lexer<>(Definitions.Logic.LeftParenthesis, "\\("),
      new Lexer<>(Definitions.Logic.RightParenthesis, "\\)"),
      new Lexer<>(Definitions.Logic.Query, "\\?-"));
  }

  /**
   * Converts textual representation of a lexeme as a value of a token.
   *
   * @param <T> the input language
   * @param type the type of a lexeme/token
   * @param string the textual representation
   * @return the value
   */
  public static <T extends Enum<T>> Object convert(T type, String string) {
    if (type instanceof Definitions.Logic) {
      Logic logic = (Logic) type;

      switch (logic) {
        case AnonymousSymbol:
          return new Symbol(string);

        case CapitalSymbol:
          return new Symbol(string);

        case LowerSymbol:
          return new Symbol(string);

        case NotKeyword:
          return Definitions.Logic.NotKeyword;

        case Number:
          return Double.parseDouble(string);

        case String:
          return string.substring(1, string.length() - 1);

        default:
      }
    } else if (type instanceof Definitions.Graph) {
      Graph graph = (Graph) type;

      switch (graph) {
        case Symbol:
          return new Symbol(string);

        case Variable:
          return new Symbol(string);

        default:

      }
    }

    return null;
  }

  /**
   * The Enum RuleParserState are allowed states for parsing logic programs.
   */
  public static enum RuleParserState implements FinalState {

    /** The Header symbol. */
    HeaderSymbol(false),
    /** The Header opening parenthesis. */
    HeaderOpeningParenthesis(false),
    /** The Header argument. */
    HeaderArgument(false),
    /** The Header closing parenthesis or comma. */
    HeaderClosingParenthesisOrComma(false),

    /** The Left implication. */
    LeftImplication(false),

    /** The Body predicate symbol or not keyword. */
    BodyPredicateSymbolOrNotKeyword(false),
    /** The Body predicate opening parenthesis. */
    BodyPredicateOpeningParenthesis(false),
    /** The Body predicate argument. */
    BodyPredicateArgument(false),
    /** The Body predicate closing parenthesis or comma. */
    BodyPredicateClosingParenthesisOrComma(false),

    /** The Body opening parenthesis for not keyword. */
    BodyNotKeywordOpeningParenthesis(false),

    /** The Body predicate symbol after not keyword. */
    BodyNotPredicateSymbol(false),

    /** The Body predicate opening parenthesis after not keyword. */
    BodyNotPredicateOpeningParenthesis(false),

    /** The Body predicate argument after not keyword. */
    BodyNotPredicateArgument(false),

    /** The Body predicate closing parenthesis or comma after not keyword. */
    BodyNotPredicateClosingParenthesisOrComma(false),

    /** The Body closing parenthesis for not keyword. */
    BodyNotKeywordClosingParenthesis(false),

    /** The Body comma or full stop. */
    BodyCommaOrFullStop(false),
    /** The Nothing. */
    Nothing(true);

    /** The finality. */
    private final boolean finalState;

    /**
     * Instantiates a new rule parser state.
     *
     * @param finalState specifies finality
     */
    RuleParserState(boolean finalState) {
      this.finalState = finalState;
    }

    /* (non-Javadoc)
     * @see magicrule.parser.Definitions.FinalState#isFinal()
     */
    @Override
    public boolean isFinal() {
      return finalState;
    }
  }

  /**
   * The Class RuleConstructor constructs a rule during parsing of a logic program.
   */
  public static class RuleConstructor implements ElementConstructor<Rule> {

    /** The predicate symbol. */
    public Symbol predicateSymbol = null;

    /** The arguments. */
    public List<Term> arguments = new ArrayList<>();

    /** The header of a rule. */
    private Atom header = null;

    /** The body of a rule. */
    private List<Atom> body = new ArrayList<>();

    /**
     * Makes the header of a rule.
     */
    public void makeHeader() {
      header = new Atom(predicateSymbol, arguments, false);
      predicateSymbol = null;
      arguments = new ArrayList<>();
    }

    /**
     * Adds an atom to the body.
     */
    public void addToBody(boolean negated) {
      body.add(new Atom(predicateSymbol, arguments, negated));
      predicateSymbol = null;
      arguments = new ArrayList<>();
    }

    /* (non-Javadoc)
     * @see magicrule.parser.Definitions.ElementConstructor#construct()
     */
    @Override
    public Rule construct() {
      return new Rule(header, body);
    }
  }

  /** The transitions in the logic program parser. */
  public static Map<Pair<RuleParserState, Definitions.Logic>, Pair<RuleParserState, BiConsumer<RuleConstructor, Token<Definitions.Logic>>>> ruleParserTransitions = new HashMap<>();
  static {
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderSymbol, Definitions.Logic.LowerSymbol),
                              new Pair<>(RuleParserState.HeaderOpeningParenthesis,
                                         (rc, t) -> rc.predicateSymbol = (Symbol) t.value));
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderOpeningParenthesis, Definitions.Logic.LeftParenthesis),
                              new Pair<>(RuleParserState.HeaderArgument,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderArgument, Definitions.Logic.CapitalSymbol),
                              new Pair<>(RuleParserState.HeaderClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new NormalVariable(((Symbol)t.value).identifier))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderArgument, Definitions.Logic.Number),
                              new Pair<>(RuleParserState.HeaderClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new Constant((Double)t.value))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderArgument, Definitions.Logic.String),
                              new Pair<>(RuleParserState.HeaderClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new Constant((String)t.value))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderClosingParenthesisOrComma, Definitions.Logic.Comma),
                              new Pair<>(RuleParserState.HeaderArgument,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.HeaderClosingParenthesisOrComma, Definitions.Logic.RightParenthesis),
                              new Pair<>(RuleParserState.LeftImplication,
                                         (rc, t) -> rc.makeHeader()));
    ruleParserTransitions.put(new Pair<>(RuleParserState.LeftImplication, Definitions.Logic.LeftImplication),
                              new Pair<>(RuleParserState.BodyPredicateSymbolOrNotKeyword,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateSymbolOrNotKeyword, Definitions.Logic.LowerSymbol),
                              new Pair<>(RuleParserState.BodyPredicateOpeningParenthesis,
                                         (rc, t) -> rc.predicateSymbol = (Symbol) t.value));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateOpeningParenthesis, Definitions.Logic.LeftParenthesis),
                              new Pair<>(RuleParserState.BodyPredicateArgument,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateArgument, Definitions.Logic.CapitalSymbol),
                              new Pair<>(RuleParserState.BodyPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new NormalVariable(((Symbol)t.value).identifier))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateArgument, Definitions.Logic.AnonymousSymbol),
                              new Pair<>(RuleParserState.BodyPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new AnonymousVariable(false))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateArgument, Definitions.Logic.Number),
                              new Pair<>(RuleParserState.BodyPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new Constant((Double)t.value))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateArgument, Definitions.Logic.String),
                              new Pair<>(RuleParserState.BodyPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new Constant((String)t.value))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateClosingParenthesisOrComma, Definitions.Logic.Comma),
                              new Pair<>(RuleParserState.BodyPredicateArgument,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateClosingParenthesisOrComma, Definitions.Logic.RightParenthesis),
                              new Pair<>(RuleParserState.BodyCommaOrFullStop,
                                         (rc, t) -> rc.addToBody(false)));

    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyPredicateSymbolOrNotKeyword, Definitions.Logic.NotKeyword),
                              new Pair<>(RuleParserState.BodyNotKeywordOpeningParenthesis,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotKeywordOpeningParenthesis, Definitions.Logic.LeftParenthesis),
                              new Pair<>(RuleParserState.BodyNotPredicateSymbol,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateSymbol, Definitions.Logic.LowerSymbol),
                              new Pair<>(RuleParserState.BodyNotPredicateOpeningParenthesis,
                                         (rc, t) -> rc.predicateSymbol = (Symbol) t.value));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateOpeningParenthesis, Definitions.Logic.LeftParenthesis),
                              new Pair<>(RuleParserState.BodyNotPredicateArgument,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateArgument, Definitions.Logic.CapitalSymbol),
                              new Pair<>(RuleParserState.BodyNotPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new NormalVariable(((Symbol)t.value).identifier))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateArgument, Definitions.Logic.AnonymousSymbol),
                              new Pair<>(RuleParserState.BodyNotPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new AnonymousVariable(false))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateArgument, Definitions.Logic.Number),
                              new Pair<>(RuleParserState.BodyNotPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new Constant((Double)t.value))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateArgument, Definitions.Logic.String),
                              new Pair<>(RuleParserState.BodyNotPredicateClosingParenthesisOrComma,
                                         (rc, t) -> rc.arguments.add(new Constant((String)t.value))));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateClosingParenthesisOrComma, Definitions.Logic.Comma),
                              new Pair<>(RuleParserState.BodyNotPredicateArgument,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotPredicateClosingParenthesisOrComma, Definitions.Logic.RightParenthesis),
                              new Pair<>(RuleParserState.BodyNotKeywordClosingParenthesis,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyNotKeywordClosingParenthesis, Definitions.Logic.RightParenthesis),
                              new Pair<>(RuleParserState.BodyCommaOrFullStop,
                                         (rc, t) -> rc.addToBody(true)));

    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyCommaOrFullStop, Definitions.Logic.Comma),
                              new Pair<>(RuleParserState.BodyPredicateSymbolOrNotKeyword,
                                         (rc, t) -> {}));
    ruleParserTransitions.put(new Pair<>(RuleParserState.BodyCommaOrFullStop, Definitions.Logic.FullStop),
                              new Pair<>(RuleParserState.Nothing,
                                         (rc, t) -> {}));
  }

  /**
   * The Enum Graph is family of types for lexemes and tokens of SIP graphs.
   */
  public static enum Graph {

    /** The Left brace. */
    LeftBrace,
    /** The Symbol. */
    Symbol,
    /** The Variable. */
    Variable,
    /** The Comma. */
    Comma,
    /** The Semicolon. */
    Semicolon,
    /** The Right brace. */
    RightBrace,
    /** The Arrow body. */
    ArrowBody,
    /** The Arrow tip. */
    ArrowTip
  }

  /** The lexers for recognizing lexemes in graph descriptions. */
  public static Set<Lexer<Graph>> graphLexers = new HashSet<>();
  static {
    Collections.addAll(
      graphLexers,
      new Lexer<>(null, "\\p{javaWhitespace}+|\\(no edges\\)"), // Lexer for fake lexemes
      new Lexer<>(Definitions.Graph.LeftBrace, "\\{"),
      new Lexer<>(Definitions.Graph.Symbol, "[a-z][a-zA-Z0-9_\\.]*"),
      new Lexer<>(Definitions.Graph.Variable, "[A-Z][a-zA-Z0-9_]*"),
      new Lexer<>(Definitions.Graph.Comma, ","),
      new Lexer<>(Definitions.Graph.Semicolon, ";"),
      new Lexer<>(Definitions.Graph.RightBrace, "\\}"),
      new Lexer<>(Definitions.Graph.ArrowBody, "-{2,}+(?!>)"),
      new Lexer<>(Definitions.Graph.ArrowTip, "-{2,}>"));
  }

  /**
   * The Interface FinalState.
   */
  public static interface FinalState {

    /**
     * Checks if the state is final.
     *
     * @return true, if the state is final
     */
    public boolean isFinal();
  }

  /**
   * The Enum GraphParserState are allowed states for parsing graph descriptions.
   */
  public static enum GraphParserState implements FinalState {

    /** The Path opening brace. */
    PathOpeningBrace(true),
    /** The Path atom. */
    PathAtom(false),
    /** The Path comma or semicolon or closing brace. */
    PathCommaOrSemicolonOrClosingBrace(false),

    /** The Path atom after semicolon. */
    PathAtomAfterSemicolon(false),
    /** The Path closing brace. */
    PathClosingBrace(false),
    /** The Arrow body. */
    ArrowBody(false),
    /** The Arrow symbol. */
    ArrowSymbol(false),

    /** The Arrow tip or comma. */
    ArrowTipOrComma(false),
    /** The Atom after tip. */
    AtomAfterTip(false),
    /** The Nothing or path opening brace. */
    NothingOrPathOpeningBrace(true);

    /** The finality. */
    private final boolean finalState;

    /**
     * Instantiates a new graph parser state.
     *
     * @param finalState the finality
     */
    GraphParserState(boolean finalState) {
      this.finalState = finalState;
    }

    /* (non-Javadoc)
     * @see magicrule.parser.Definitions.FinalState#isFinal()
     */
    @Override
    public boolean isFinal() {
      return finalState;
    }
  }

  /**
   * The Class RuleConstructor constructs a SIP graph during parsing of a graph description.
   */
  public static class GraphConstructor implements ElementConstructor<SIPGraph> {

    /** The resulting graph. */
    private SIPGraph graph = new SIPGraph();

    /** The initial path from the header atom. */
    public List<Symbol> path = new ArrayList<>();

    /** The weight of an edge (passing information)s. */
    public List<NormalVariable> passingInformation = new ArrayList<>();

    /** The tail of an edge. */
    public Symbol targetSymbol = null;

    /**
     * Adds the edge to the graph.
     */
    public void addEdge() {
      graph.addPassingInformation(path.get(path.size() - 1), targetSymbol, new HashSet<>(passingInformation));
      path = new ArrayList<>();
      passingInformation = new ArrayList<>();
      targetSymbol = null;
    }

    /* (non-Javadoc)
     * @see magicrule.parser.Definitions.ElementConstructor#construct()
     */
    public SIPGraph construct() {
      return graph;
    }
  }

  /** The transitions in the SIP graph parser. */
  public static Map<Pair<GraphParserState, Definitions.Graph>, Pair<GraphParserState, BiConsumer<GraphConstructor, Token<Definitions.Graph>>>> graphParserTransitions = new HashMap<>();
  static {
    graphParserTransitions.put(new Pair<>(GraphParserState.PathOpeningBrace, Definitions.Graph.LeftBrace),
                               new Pair<>(GraphParserState.PathAtom,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.PathAtom, Definitions.Graph.Symbol),
                               new Pair<>(GraphParserState.PathCommaOrSemicolonOrClosingBrace,
                                          (gc, t) -> gc.path.add((Symbol)t.value)));
    graphParserTransitions.put(new Pair<>(GraphParserState.PathCommaOrSemicolonOrClosingBrace, Definitions.Graph.Comma),
                               new Pair<>(GraphParserState.PathAtom,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.PathCommaOrSemicolonOrClosingBrace, Definitions.Graph.Semicolon),
                               new Pair<>(GraphParserState.PathAtomAfterSemicolon,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.PathAtomAfterSemicolon, Definitions.Graph.Symbol),
                               new Pair<>(GraphParserState.PathClosingBrace,
                                          (gc, t) -> gc.path.add((Symbol)t.value)));
    graphParserTransitions.put(new Pair<>(GraphParserState.PathCommaOrSemicolonOrClosingBrace, Definitions.Graph.RightBrace),
                               new Pair<>(GraphParserState.ArrowBody,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.PathClosingBrace, Definitions.Graph.RightBrace),
                               new Pair<>(GraphParserState.ArrowBody,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.ArrowBody, Definitions.Graph.ArrowBody),
                               new Pair<>(GraphParserState.ArrowSymbol,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.ArrowSymbol, Definitions.Graph.Variable),
                               new Pair<>(GraphParserState.ArrowTipOrComma,
                                          (gc, t) -> gc.passingInformation.add(new NormalVariable(((Symbol)t.value).identifier))));
    graphParserTransitions.put(new Pair<>(GraphParserState.ArrowTipOrComma, Definitions.Graph.Comma),
                               new Pair<>(GraphParserState.ArrowSymbol,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.ArrowTipOrComma, Definitions.Graph.ArrowTip),
                               new Pair<>(GraphParserState.AtomAfterTip,
                                          (gc, t) -> {}));
    graphParserTransitions.put(new Pair<>(GraphParserState.AtomAfterTip, Definitions.Graph.Symbol),
                               new Pair<>(GraphParserState.NothingOrPathOpeningBrace,
    (gc, t) -> {
      gc.targetSymbol = (Symbol)t.value;
      gc.addEdge();
    }));
    graphParserTransitions.put(new Pair<>(GraphParserState.NothingOrPathOpeningBrace, Definitions.Graph.LeftBrace),
                               new Pair<>(GraphParserState.PathAtom,
                                          (gc, t) -> {}));
  }
}
