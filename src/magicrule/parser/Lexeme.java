package magicrule.parser;

/**
 * The Class Lexeme represents lexemes of the given language in the input.
 *
 * @param <T> the language being parsed
 */
public class Lexeme<T extends Enum<T>> {

  /** The type of a lexeme in the given language. */
  public final T type;

  /** The string representing a lexeme. */
  public final String string;

  /** The position of a lexeme in the input. */
  public final Position position;

  /**
   * Instantiates a new lexeme.
   *
   * @param type the type of a lexeme
   * @param string the string representation
   * @param position the position in the input
   */
  Lexeme(T type, String string, Position position) {
    this.type = type;
    this.string = string;
    this.position = position;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Lexeme<type: " + type + ", string: \"" + string + "\", position: " + position + ">";
  }
}
