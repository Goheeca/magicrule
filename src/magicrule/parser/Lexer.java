package magicrule.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The Class Lexer is reading an input and checking for lexemes.
 *
 * @param <T> the language being parsed
 */
public class Lexer<T extends Enum<T>> {

  /** The type of a specific lexeme. */
  private final T type;

  /** The pattern matching a lexeme. */
  private final Pattern pattern;

  /** The matcher doing the recognition. */
  private Matcher lookingAt;

  /**
   * Instantiates a new lexer.
   *
   * @param type the type of the lexeme
   * @param regex the regex matching the specific lexeme
   */
  Lexer(T type, String regex) {
    this.type = type;
    this.pattern = Pattern.compile(regex);
  }

  /**
   * Returns whether is the given type of a lexeme present at the start of an input.
   *
   * @param input the input being probed
   * @return true, if successful
   */
  private boolean match(StringBuilder input) {
    Matcher matcher = pattern.matcher(input);
    boolean ret = matcher.lookingAt();
    lookingAt = ret ? matcher : null;
    return ret;
  }

  /**
   * Slurps an lexeme from the input.
   *
   * @param input the input
   * @param position the current position of the start of the input
   * @return the lexeme
   */
  private Lexeme<T> lex(StringBuilder input, int position) {
    if (lookingAt == null)
      throw new RuntimeException("Lexer hasn't been matched.");

    String string = input.substring(lookingAt.start(), lookingAt.end());
    input.delete(lookingAt.start(), lookingAt.end());
    return new Lexeme<>(type, string, new Position(position));
  }

  /** The Constant EXCERPT_LENGTH is used for exception messages. */
  private static final int EXCERPT_LENGTH = 20;

  /**
   * Returns a next lexeme in the input.
   *
   * @param <T> the language being parsed
   * @param lexers the lexers used for recognition
   * @param input the input
   * @param originalLength the original length of the input as whole
   * @return the lexeme
   */
  private static <T extends Enum<T>> Lexeme<T> getLexeme(Set<Lexer<T>> lexers, StringBuilder input, int originalLength) {
    Map<Boolean, List<Lexer<T>>> lexings = lexers.stream()
                                           .collect(Collectors.partitioningBy(lexer -> lexer.match(input)));
    int position = originalLength - input.length();

    if (lexings.get(true).size() == 0)
      throw new RuntimeException("Invalid lexeme (@" + position + "): " + input.substring(0, Math.min(EXCERPT_LENGTH, input.length())));

    if (lexings.get(true).size() > 1)
      throw new RuntimeException("Ambiguous lexeme (@" + position + "): " + input.substring(0, Math.min(EXCERPT_LENGTH, input.length())));

    return lexings.get(true).get(0).lex(input, position);
  }

  /**
   * Converts the input into the lexemes.
   *
   * @param <T> the language being parsed
   * @param lexers the lexers used for recognition
   * @param input the input
   * @return the list of lexemes
   */
  public static <T extends Enum<T>> List<Lexeme<T>> lexicalize(Set<Lexer<T>> lexers, StringBuilder input) {
    List<Lexeme<T>> ret = new ArrayList<>();
    int length = input.length();

    while (input.length() > 0) {
      ret.add(getLexeme(lexers, input, length));
    }

    // Filter only true lexemes
    ret = ret.stream()
          .filter(lexeme -> lexeme.type != null)
          .collect(Collectors.toList());
    return ret;
  }
}
