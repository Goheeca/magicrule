package magicrule.parser;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import magicrule.Pair;
import magicrule.parser.Definitions.ElementConstructor;
import magicrule.parser.Definitions.FinalState;

/**
 * The Class Parser parses the input in the given language.
 */
public class Parser {

  /**
   * Parses the input.
   *
   * @param <Element> the abstract representation of the input
   * @param <ConcreteConstructor> the type of a builder
   * @param <ParserState> the allowed states
   * @param <TokenFamily> the input language
   * @param concreteConstructor the builder of the abstract representation
   * @param initialState the initial state of the finite state automata
   * @param transitions the transitions of the finite state automata
   * @param tokens the input tokens
   * @return the parsed element
   */
  public static <Element, ConcreteConstructor extends ElementConstructor<Element>, ParserState extends Enum<ParserState> & FinalState, TokenFamily extends Enum<TokenFamily>>
  Element parse(ConcreteConstructor concreteConstructor,
                ParserState initialState,
                Map<Pair<ParserState, TokenFamily>, Pair<ParserState, BiConsumer<ConcreteConstructor, Token<TokenFamily>>>> transitions,
                List<Token<TokenFamily>> tokens) {
    ParserState state = initialState;

    while (!tokens.isEmpty()) {
      Token<TokenFamily> token = tokens.remove(0);
      Pair<ParserState, TokenFamily> key = new Pair<>(state, token.type);

      if (!transitions.containsKey(key)) {
        throw new RuntimeException("Bad constructed input data, expected a token representing: " + state + ", given a token: " + token.type);
      }

      Pair<ParserState, BiConsumer<ConcreteConstructor, Token<TokenFamily>>> transition = transitions.get(key);
      state = transition.first;
      transition.second.accept(concreteConstructor, token);
    }

    if (!state.isFinal()) {
      throw new RuntimeException("The state expecting: " + state + ", is not final.");
    }

    return concreteConstructor.construct();
  }
}
