package magicrule.parser;

/**
 * The Class Position is holding information about position in the chunk which is being parsed.
 */
public class Position {

  /** The position is plain position from the start. */
  public final long position;

  /**
   * Instantiates a new position.
   *
   * @param position the position from the start
   */
  Position(long position) {
    this.position = position;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return Long.toString(position);
  }
}
