package magicrule.parser;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The Class Token represents tokens of the given language.
 *
 * @param <T> the language being parsed
 */
public class Token<T extends Enum<T>> {

  /** The type of a token in the given language. */
  public final T type;

  /** The lexeme corresponding to this token. */
  public final Lexeme<T> lexeme;

  /** The value of this token. */
  public final Object value;

  /**
   * Instantiates a new token.
   *
   * @param type the token type
   * @param lexeme the corresponding lexeme
   * @param value the value of this token
   */
  private Token(T type, Lexeme<T> lexeme, Object value) {
    this.type = type;
    this.lexeme = lexeme;
    this.value = value;
  }

  /**
   * Converts a lexeme into the token, transform textual representation into the value of a token.
   *
   * @param <T> the language being parsed
   * @param lexeme the corresponding lexeme
   * @return the token
   */
  private static <T extends Enum<T>> Token<T> token(Lexeme<T> lexeme) {
    return new Token<>(lexeme.type, lexeme, Definitions.convert(lexeme.type, lexeme.string));
  }

  /**
   * Converts lexemes into the tokens.
   *
   * @param <T> the generic type
   * @param lexemes the lexemes
   * @return the list
   */
  public static <T extends Enum<T>> List<Token<T>> tokenize(List<Lexeme<T>> lexemes) {
    return lexemes.stream().map(Token::token).collect(Collectors.toList());
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    String val = "";

    if (value != null) val = ", value: " + value;

    return "Token<type: " + type + val + ">";
  }
}
