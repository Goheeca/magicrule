package magicrule.reader;

import java.util.Optional;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;

import magicrule.PartType;

/**
 * The Class Partitioner is for splitting the input up.
 */
public class Partitioner {

  /** The Constant partDelimiter defines delimiter of sections in the input. */
  public static final String partDelimiter = "\\R{3,}|\\R*\\Z"; // double blank line

  /** The Constant contentDelimiter defines delimiter between a header and a content of a section. */
  public static final String contentDelimiter = "\\R{2}"; // single blank line

  /**
   * The Class Part represents particular sections.
   */
  public static class Part {

    /** The part type. */
    public Optional<PartType> type = Optional.empty();

    /** The header. */
    public final String header;

    /** The content. */
    public final String content;

    /**
     * Instantiates a new part.
     *
     * @param header the header
     * @param content the content
     */
    public Part(String header, String content) {
      this.header = header;
      this.content = content;
    }

    /**
     * Instantiates a new part.
     *
     * @param chunk the chunk which contains a header and a content
     */
    public Part(String chunk) {
      String[] parts = chunk.split(contentDelimiter, 2);
      header = parts[0];
      content = (parts.length < 2) ? "" : parts[1];
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      return "Part<header: '" + header + "', content: '" + content + "'>";
    }
  }

  /**
   * Splits up the input into the sections.
   *
   * @param is the input
   * @return the list of sections
   */
  public static List<Part> partition(InputStream is) {
    try(Scanner sc = new Scanner(is)) {
      sc.useDelimiter(partDelimiter);
      List<Part> parts = new ArrayList<>();

      while (sc.hasNext()) {
        parts.add(new Part(sc.next()));
      }

      return parts;
    }
  }
}
