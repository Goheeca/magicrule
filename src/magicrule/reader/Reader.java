package magicrule.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;


import magicrule.PartType;
import magicrule.Record;
import magicrule.reader.Partitioner.Part;

/**
 * The Class Reader loads a Record from the input.
 */
public class Reader {

  /**
   * reads a Record.
   *
   * @param file the input file
   * @return the loaded record
   */
  public static Record readRecord(File file) {
    try {
      List<Part> parts = Partitioner.partition(new FileInputStream(file));
      Optional<Part> adornedRule = parts.stream()
                                   .filter(part ->
                                           PartType.ADORNED_RULE.pattern
                                           .asPredicate()
                                           .test(part.header))
                                   .findFirst();
      Optional<Part> SIP = parts.stream()
                           .filter(part ->
                                   PartType.SIP.pattern
                                   .asPredicate()
                                   .test(part.header))
                           .findFirst();
      SIP.get().type = Optional.of(PartType.SIP);
      adornedRule.get().type = Optional.of(PartType.ADORNED_RULE);

      return new Record(parts, SIP.get().content, adornedRule.get().content);
    } catch (NoSuchElementException e) {
      throw new RuntimeException("Missing a part in a record", e);
    } catch (IOException e) {
      throw new RuntimeException("IO", e);
    }
  }
}
