package magicrule.writer;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Optional;
import java.util.stream.Collectors;

import magicrule.PartType;
import magicrule.Record;

/**
 * The Class Writer provides means for outputting Records to an file.
 */
public class Writer {

  /** The Constant newLine used as a line separator. */
  public static final String newLine = System.getProperty("line.separator");
  
  /** Settings what to output. */
  public static boolean allOutput = true;

  /**
   * Returns placeholder instead of an empty content for the particular section.
   *
   * @param partType the part type
   * @return the string
   */
  private static String emptyContent(PartType partType) {
    switch (partType) {
      case ADORNED_RULE:
      case MODIFIED_RULE:
      case ORIGINAL_RULE:
        return "(no rule)";

      case MAGIC_RULES:
        return "(no rules)";

      case FSIP:
      case SIP:
        return "(no edges)";

      default:
        return "";

    }
  }

  private static String content(Optional<PartType> type, String content) {
    if (content.equals("")) {
      return emptyContent(type.get());
    } else {
      return content;
    }
  }

  /**
   * Writes record to the file.
   *
   * @param file the output file
   * @param record the record being written
   */
  public static void writeRecord(File file, Record record) {
    try(PrintStream ps = new PrintStream(file)) {
      ps.print(record.parts.stream()
    		   .filter(part -> allOutput || (part.type.isPresent() && part.type.get().equals(PartType.MAGIC_RULES)))
               .map(part -> part.header + newLine + newLine + content(part.type, part.content))
               .collect(Collectors.joining(newLine + newLine + newLine)));
    } catch (IOException e) {
      throw new RuntimeException("IO", e);
    }
  }
}
