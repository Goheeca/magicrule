package magicrule;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  PairTest.class,
  PartTypeTest.class,
  UtilTest.class,
  RecordTest.class
})
public class MainSuite {
}