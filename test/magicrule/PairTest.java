package magicrule;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PairTest {
  Pair<?, ?> pair;

  @Before
  public void setUp() {
    pair = new Pair<>(1, "string");
  }

  @Test
  public void testFirst() {
    assertEquals(1, pair.first);
  }

  @Test
  public void testSecond() {
    assertEquals("string", pair.second);
  }

  @Test
  public void testHash() {
    Pair<?, ?> pair1 = new Pair<>("abc", 123);
    Pair<?, ?> pair2 = new Pair<>("abc", 123);
    assertEquals(pair1.hashCode(), pair2.hashCode());
  }

  @Test
  public void testEquality0() {
    assertEquals(true, pair.equals(pair));
  }

  @Test
  public void testEquality1() {
    Pair<?, ?> pair1 = new Pair<>("abc", 123);
    Pair<?, ?> pair2 = new Pair<>("abc", 123);
    assertTrue(pair1.equals(pair2));
  }

  @Test
  public void testEquality2() {
    Pair<?, ?> pair1 = new Pair<>("abc", 123);
    Pair<?, ?> pair2 = new Pair<>(123, "abc");
    assertFalse(pair1.equals(pair2));
  }

  @Test
  public void testEquality3() {
    assertFalse(pair.equals(null));
  }

  @Test
  public void testEquality4() {
    assertFalse(pair.equals("abc"));
  }

  @Test
  public void testEquality5() {
    Pair<?, ?> pair1 = new Pair<>("abc", 123);
    Pair<?, ?> pair2 = new Pair<>("abc", 1234);
    assertFalse(pair1.equals(pair2));
  }

  @Test
  public void testEquality6() {
    Pair<?, ?> pair1 = new Pair<>("abc", 123);
    Pair<?, ?> pair2 = new Pair<>("abcd", 123);
    assertFalse(pair1.equals(pair2));
  }
}
