package magicrule;

import static org.junit.Assert.*;

import java.util.regex.Matcher;

import org.junit.Test;

public class PartTypeTest {

  @Test
  public void testOriginalRule() {
    String header = "Original rule:";
    Matcher m = PartType.ORIGINAL_RULE.pattern.matcher(header);
    assertTrue(m.find());
  }

  @Test
  public void testModifiedRule0() {
    String header = "Modified rule: (bbfbfb)";
    Matcher m = PartType.MODIFIED_RULE.pattern.matcher(header);
    assertTrue(m.find());
  }

  @Test
  public void testModifiedRule1() {
    String header = "Modified rule: ()";
    Matcher m = PartType.MODIFIED_RULE.pattern.matcher(header);
    assertFalse(m.find());
  }

  @Test
  public void testModifiedRule2() {
    String header = "Modified rule: (abcdef)";
    Matcher m = PartType.MODIFIED_RULE.pattern.matcher(header);
    assertFalse(m.find());
  }

  @Test
  public void testSIP() {
    String header = "SIP:";
    Matcher m = PartType.SIP.pattern.matcher(header);
    assertTrue(m.find());
  }

  @Test
  public void testFSIP() {
    String header = "FSIP:";
    Matcher m = PartType.FSIP.pattern.matcher(header);
    assertTrue(m.find());
  }

  @Test
  public void testAdornedRule() {
    String header = "Adorned rule:";
    Matcher m = PartType.ADORNED_RULE.pattern.matcher(header);
    assertTrue(m.find());
  }

  @Test
  public void testMagicRules() {
    String header = "Magic rules:";
    Matcher m = PartType.MAGIC_RULES.pattern.matcher(header);
    assertTrue(m.find());
  }
}
