package magicrule;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import magicrule.reader.Partitioner.Part;

public class RecordTest {
  List<Part> parts;
  String SIP, rule;

  @Before
  public void setUp() {
    String nl = System.getProperty("line.separator");
    parts = new ArrayList<>();
    parts.add(new Part(
                "Original rule:" + nl
                + nl
                + "preceding_sibling(Line1, Line2, Element1, Element2) :-" + nl
                + "  self(Line1, Element1)," + nl
                + "  xml(Line1, Element1, Order1, Level)," + nl
                + "  self(Line2, Element2)," + nl
                + "  xml(Line2, Element2, Order2, Level)," + nl
                + "  lt(Order2, Order1)," + nl
                + "  parent(Line1, Line3, Element1, Element3)," + nl
                + "  parent(Line2, Line3, Element2, Element3)." + nl
              ));
    parts.add(new Part(
                "Modified rule: (bbbb)" + nl
                + nl
                + "preceding_sibling_h(Line1, Line2, Element1, Element2) :-" + nl
                + "  self.1(Line1, Element1)," + nl
                + "  xml.1(Line1, Element1, Order1, Level)," + nl
                + "  self.2(Line2, Element2)," + nl
                + "  xml.2(Line2, Element2, Order2, Level)," + nl
                + "  lt.1(Order2, Order1)," + nl
                + "  parent.1(Line1, Line3, Element1, Element3)," + nl
                + "  parent.2(Line2, Line3, Element2, Element3)." + nl
              ));
    Part partSIP = new Part(
      "SIP:" + nl
      + nl
      + "{preceding_sibling_h} ---------------- Line1, Element1 --> self.1" + nl
      + "{preceding_sibling_h} ---------------- Line1, Element1 --> xml.1" + nl
      + "{preceding_sibling_h} ---------------- Line2, Element2 --> self.2" + nl
      + "{preceding_sibling_h} ---------------- Line2, Element2 --> xml.2" + nl
      + "{preceding_sibling_h} ---------------- Line1, Element1 --> parent.1" + nl
      + "{preceding_sibling_h} ---------------- Line2, Element2 --> parent.2" + nl
      + "{preceding_sibling_h; xml.1} --------- Level ------------> xml.2" + nl
      + "{preceding_sibling_h; xml.1} --------- Order1 -----------> lt.1" + nl
      + "{preceding_sibling_h, xml.1; xml.2} -- Order2 -----------> lt.1" + nl
      + "{preceding_sibling_h; parent.1} ------ Line3, Element3 --> parent.2" + nl
    );
    SIP = partSIP.content;
    parts.add(partSIP);
    parts.add(new Part(
                "FSIP:" + nl
                + nl
                + "{preceding_sibling_h} ------------ Line1, Element1 --> self.1" + nl
                + "{preceding_sibling_h} ------------ Line2, Element2 --> self.2" + nl
                + "{preceding_sibling_h} ------------ Line1, Element1 --> parent.1" + nl
                + "{preceding_sibling_h} ------------ Line2, Element2 --> parent.2" + nl
                + "{preceding_sibling_h; parent.1} -- Line3, Element3 --> parent.2" + nl
              ));
    Part partRule = new Part(
      "Adorned rule:" + nl
      + nl
      + "preceding_sibling_bbbb(Line1, Line2, Element1, Element2) :-" + nl
      + "  self_bb(Line1, Element1)," + nl
      + "  xml(Line1, Element1, Order1, Level)," + nl
      + "  self_bb(Line2, Element2)," + nl
      + "  xml(Line2, Element2, Order2, Level)," + nl
      + "  lt(Order2, Order1)," + nl
      + "  parent_bfbf(Line1, Line3, Element1, Element3)," + nl
      + "  parent_bbbb(Line2, Line3, Element2, Element3)." + nl
    );
    rule = partRule.content;
    parts.add(partRule);
  }

  @Test
  public void testParsing() {
    @SuppressWarnings("unused")
    Record record = new Record(parts, SIP, rule);
  }

  @Test
  public void testGenerateMagicForm() {
    String nl = System.getProperty("line.separator");
    Record record = new Record(parts, SIP, rule);
    record.generateMagicForm();
    Part partMagic = record.parts.get(record.parts.size() - 1);
    String magicRule =
      "m_self_bb(Line1, Element1) :-" + nl
      + "  m_preceding_sibling_bbbb(Line1, Line2, Element1, Element2)." + nl
      + nl
      + "m_self_bb(Line2, Element2) :-" + nl
      + "  m_preceding_sibling_bbbb(Line1, Line2, Element1, Element2)." + nl
      + nl
      + "m_parent_bfbf(Line1, Element1) :-" + nl
      + "  m_preceding_sibling_bbbb(Line1, Line2, Element1, Element2)." + nl
      + nl
      + "m_parent_bbbb(Line2, Line3, Element2, Element3) :-" + nl
      + "  m_preceding_sibling_bbbb(Line1, Line2, Element1, Element2)," + nl
      + "  m_parent_bfbf(Line1, Element1)," + nl
      + "  parent_bfbf(Line1, Line3, Element1, Element3).";

    assertEquals("Magic rules:", partMagic.header);
    assertEquals(magicRule, partMagic.content);
  }
}
