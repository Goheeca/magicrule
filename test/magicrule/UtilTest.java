package magicrule;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.Test;

public class UtilTest {

  @Test
  public void testCounter0() {
    Supplier<Integer> c = Util.counter(1);
    assertEquals(1, c.get().intValue());
    assertEquals(2, c.get().intValue());
    assertEquals(3, c.get().intValue());
  }

  @Test
  public void testCounter1() {
    Supplier<Integer> c = Util.counter(123);
    assertEquals(123, c.get().intValue());
    assertEquals(124, c.get().intValue());
    assertEquals(125, c.get().intValue());
    assertEquals(126, c.get().intValue());
  }

  @Test
  public void testZip0() {
    Stream<Integer> s = Util.zip(Stream.generate(Util.counter(0)), Stream.generate(Util.counter(100)), (a, b) -> a + b);
    Iterator<Integer> it = s.iterator();
    assertEquals(100, it.next().intValue());
    assertEquals(102, it.next().intValue());
    assertEquals(104, it.next().intValue());
  }

  @Test
  public void testZip1() {
    List<Integer> list1 = Arrays.asList(1, 2, 3);
    List<Integer> list2 = Arrays.asList(10, 20, 30, 40, 50);
    Stream<Integer> s = Util.zip(list1.stream(), list2.stream(), (a, b) -> a + b);
    Iterator<Integer> it = s.iterator();
    assertEquals(11, it.next().intValue());
    assertEquals(22, it.next().intValue());
    assertEquals(33, it.next().intValue());
    assertFalse(it.hasNext());
  }

  @Test
  public void testZip2() {
    List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5);
    List<Integer> list2 = Arrays.asList(10, 20, 30, 40);
    Stream<Integer> s = Util.zip(list1.stream(), list2.stream(), (a, b) -> a + b);
    Iterator<Integer> it = s.iterator();
    assertEquals(11, it.next().intValue());
    assertEquals(22, it.next().intValue());
    assertEquals(33, it.next().intValue());
    assertEquals(44, it.next().intValue());
    assertFalse(it.hasNext());
  }

  @Test
  public void testZip3() {
    List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5);
    List<Integer> list2 = Arrays.asList(10, 20, 30, 40);
    Stream<Integer> s = Util.zip(list1.stream().parallel(), list2.stream().parallel(), (a, b) -> a + b);
    assertTrue(s.isParallel());
  }

  @Test
  public void testZip4() {
    List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5);
    List<Integer> list2 = Arrays.asList(10, 20, 30, 40);
    Stream<Integer> s = Util.zip(list1.stream(), list2.stream().parallel(), (a, b) -> a + b);
    assertTrue(s.isParallel());
  }
}
