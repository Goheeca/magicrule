package magicrule;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import magicrule.graph.GraphSuite;
import magicrule.logic.LogicSuite;
import magicrule.parser.ParserSuite;
import magicrule.reader.ReaderSuite;
import magicrule.writer.WriterSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  MainSuite.class,
  ReaderSuite.class,
  WriterSuite.class,
  GraphSuite.class,
  LogicSuite.class,
  ParserSuite.class
})
public class WholeSuite {
}
