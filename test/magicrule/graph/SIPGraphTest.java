package magicrule.graph;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import magicrule.logic.NormalVariable;
import magicrule.logic.Symbol;

public class SIPGraphTest {
  SIPGraph graph;
  Symbol a, b, c, d;
  NormalVariable x, y, z;

  @Before
  public void setUp() {
    graph = new SIPGraph();
    a = new Symbol("a");
    b = new Symbol("b");
    c = new Symbol("c");
    d = new Symbol("d");
    x = new NormalVariable("x");
    y = new NormalVariable("y");
    z = new NormalVariable("z");
  }

  @Test
  public void test1() {
    Set<NormalVariable> pXY = new HashSet<NormalVariable>(Arrays.asList(x, y));
    Set<NormalVariable> pX = new HashSet<NormalVariable>(Arrays.asList(x));
    Set<NormalVariable> pZ = new HashSet<NormalVariable>(Arrays.asList(z));
    graph.addPassingInformation(a, b, pXY);
    graph.addPassingInformation(a, c, pX);
    graph.addPassingInformation(b, d, pZ);
    assertEquals(new PassingInformation(pZ, b, d), graph.withTail(d).iterator().next());
  }

  @Test
  public void test2() {
    Set<NormalVariable> pXY = new HashSet<NormalVariable>(Arrays.asList(x, y));
    Set<NormalVariable> pX = new HashSet<NormalVariable>(Arrays.asList(x));
    Set<NormalVariable> pZ = new HashSet<NormalVariable>(Arrays.asList(z));
    graph.addPassingInformation(a, b, pXY);
    graph.addPassingInformation(a, c, pX);
    graph.addPassingInformation(b, d, pZ);
    assertEquals(new PassingInformation(pZ, b, d), graph.withHead(b).iterator().next());
  }

  @Test
  public void test3() {
    Set<NormalVariable> pXY = new HashSet<NormalVariable>(Arrays.asList(x, y));
    Set<NormalVariable> pX = new HashSet<NormalVariable>(Arrays.asList(x));
    Set<NormalVariable> pZ = new HashSet<NormalVariable>(Arrays.asList(z));
    graph.addPassingInformation(a, b, pXY);
    graph.addPassingInformation(a, c, pX);
    graph.addPassingInformation(b, d, pZ);
    Iterator<PassingInformation> it = graph.withHead(a).iterator();
    assertEquals(new PassingInformation(pXY, a, b), it.next());
    assertEquals(new PassingInformation(pX, a, c), it.next());
  }
}
