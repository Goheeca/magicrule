package magicrule.logic;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnonymousVariableTest {
  @Test
  public void test1() {
    assertEquals(new AnonymousVariable(false), new AnonymousVariable(false));
  }

  @Test
  public void test2() {
    assertNotEquals(new AnonymousVariable(true), new AnonymousVariable(true));
  }

  @Test
  public void test3() {
    assertEquals("_", new AnonymousVariable(true).toString());
  }
}
