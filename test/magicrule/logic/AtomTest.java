package magicrule.logic;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class AtomTest {
  List<Term> terms1, terms2, terms3;

  @Before
  public void setUp() {
    terms1 = Arrays.asList(
               new NormalVariable("abc"),
               new NormalVariable("def"),
               new NormalVariable("ghi"));
    terms2 = Arrays.asList(
               new NormalVariable("abc"),
               new NormalVariable("ghi"));
    terms3 = Arrays.asList(
               new NormalVariable("abc"),
               new NormalVariable("ghi"));
  }

  @Test
  public void test1() {
    assertEquals(new Atom(new Symbol("p"), terms2, false), new Atom(new Symbol("p"), terms3, false));
  }

  @Test
  public void test2() {
    assertNotEquals(new Atom(new Symbol("p"), terms2, false), new Atom(new Symbol("p"), terms2, true));
  }

  @Test
  public void test3() {
    assertNotEquals(new Atom(new Symbol("p"), terms2, false), new Atom(new Symbol("q"), terms2, false));
  }

  @Test
  public void test4() {
    assertNotEquals(new Atom(new Symbol("p"), terms1, false), new Atom(new Symbol("p"), terms2, false));
  }
}
