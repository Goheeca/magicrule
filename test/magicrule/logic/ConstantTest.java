package magicrule.logic;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConstantTest {
  @Test
  public void test1() {
    Constant n1 = new Constant(123.4);
    Constant n2 = new Constant(123.4);
    assertEquals(n1, n2);
  }

  @Test
  public void test2() {
    Constant s1 = new Constant("ab");
    Constant s2 = new Constant("ab");
    assertEquals(s1, s2);
  }

  @Test
  public void test3() {
    Constant n1 = new Constant(100.0);
    Constant s2 = new Constant("ab");
    assertNotEquals(n1, s2);
  }

  @Test
  public void test4() {
    Constant n1 = new Constant(100.0);
    Constant n2 = new Constant(-2.1);
    assertNotEquals(n1, n2);
  }

  @Test
  public void test5() {
    Constant s1 = new Constant("cd");
    Constant s2 = new Constant("ab");
    assertNotEquals(s1, s2);
  }
}
