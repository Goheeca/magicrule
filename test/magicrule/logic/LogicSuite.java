package magicrule.logic;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  AnonymousVariableTest.class,
  NormalVariableTest.class,
  ConstantTest.class,
  SymbolTest.class,
  AtomTest.class,
  RuleTest.class
})
public class LogicSuite {
}
