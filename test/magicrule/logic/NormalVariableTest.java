package magicrule.logic;

import static org.junit.Assert.*;

import org.junit.Test;

public class NormalVariableTest {
  @Test
  public void test1() {
    assertEquals(new NormalVariable("abc"), new NormalVariable("abc"));
  }

  @Test
  public void test2() {
    assertNotEquals(new NormalVariable("abc"), new NormalVariable("def"));
  }
}
