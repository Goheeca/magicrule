package magicrule.logic;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class RuleTest {
  List<Atom> body1, body2, body3;
  Atom head1, head2;

  @Before
  public void setUp() {
    body1 = Arrays.asList(
              new Atom(new Symbol("pred"), Arrays.asList(new NormalVariable("X")), false),
              new Atom(new Symbol("qqq"), Arrays.asList(new NormalVariable("Y"), new Constant(12.4)), true));
    body2 = Arrays.asList(
              new Atom(new Symbol("pred"), Arrays.asList(new NormalVariable("X")), false),
              new Atom(new Symbol("qqq"), Arrays.asList(new NormalVariable("Y"), new Constant(12.4)), true));
    body3 = Arrays.asList(
              new Atom(new Symbol("pred"), Arrays.asList(new NormalVariable("X")), false),
              new Atom(new Symbol("zz"), Arrays.asList(new NormalVariable("Z"), new AnonymousVariable(false)), true));
    head1 = new Atom(new Symbol("rule"), Arrays.asList(new NormalVariable("W")), false);
    head1 = new Atom(new Symbol("rule"), Arrays.asList(new NormalVariable("W"), new NormalVariable("S")), false);
  }

  @Test
  public void test1() {
    assertEquals(new Rule(head1, body1), new Rule(head1, body2));
  }

  @Test
  public void test2() {
    assertNotEquals(new Rule(head1, body1), new Rule(head1, body3));
  }

  @Test
  public void test3() {
    assertNotEquals(new Rule(head1, body1), new Rule(head2, body1));
  }
}
