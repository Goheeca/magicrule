package magicrule.logic;

import static org.junit.Assert.*;

import org.junit.Test;

public class SymbolTest {
  @Test
  public void test1() {
    assertEquals(new Symbol("abc"), new Symbol("abc"));
  }

  @Test
  public void test2() {
    assertNotEquals(new Symbol("abc"), new Symbol("def"));
  }

  @Test
  public void test3() {
    assertEquals("Teds_01.1", new Symbol("Teds_01.1").toString());
  }
}
