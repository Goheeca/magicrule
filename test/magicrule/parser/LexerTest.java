package magicrule.parser;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class LexerTest {
  enum Type {
    A, B, C
  }

  StringBuilder sb;
  Lexer<Type> a, b, c;
  Set<Lexer<Type>> setFine, setAmb;

  @SuppressWarnings("serial")
  @Before
  public void setUp() {
    sb = new StringBuilder("abc123");
    a = new Lexer<>(Type.A, "[a-z]+");
    b = new Lexer<>(Type.B, "[0-9]+");
    c = new Lexer<>(Type.C, ".*");
    setFine = new HashSet<Lexer<Type>>() {{add(a); add(b);}};
    setAmb = new HashSet<Lexer<Type>>() {{add(a); add(b); add(c);}};
  }

  @Test(expected = RuntimeException.class)
  public void test0() {
    Lexer.lexicalize(setAmb, sb);
  }

  @Test
  public void test1() {
    List<Lexeme<Type>> res = Lexer.lexicalize(setFine, sb);
    assertEquals(2, res.size());
    assertEquals(0, res.get(0).position.position);
    assertEquals(3, res.get(1).position.position);
    assertEquals("abc", res.get(0).string);
    assertEquals("123", res.get(1).string);
    assertEquals(Type.A, res.get(0).type);
    assertEquals(Type.B, res.get(1).type);
  }

  @Test(expected = RuntimeException.class)
  public void test2() {
    Lexer.lexicalize(setFine, new StringBuilder("!@#"));
  }
}
