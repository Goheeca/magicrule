package magicrule.parser;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  PositionTest.class,
  LexerTest.class,
  ParserTest.class
})
public class ParserSuite {
}
