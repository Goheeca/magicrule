package magicrule.parser;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import magicrule.graph.SIPGraph;
import magicrule.logic.Symbol;

public class ParserTest {

  @Test
  public void test0() {
    StringBuilder input = new StringBuilder("");
    List<Lexeme<Definitions.Graph>> SIPLexemes = Lexer.lexicalize(Definitions.graphLexers, new StringBuilder(input));
    List<Token<Definitions.Graph>> SIPTokens = Token.tokenize(SIPLexemes);
    Parser.parse(new Definitions.GraphConstructor(), Definitions.GraphParserState.PathOpeningBrace, Definitions.graphParserTransitions, SIPTokens);
  }

  @Test
  public void test1() {
    StringBuilder input = new StringBuilder("{abc_def, ghi; jkl} -- Foo, Bar --> end");
    List<Lexeme<Definitions.Graph>> SIPLexemes = Lexer.lexicalize(Definitions.graphLexers, new StringBuilder(input));
    List<Token<Definitions.Graph>> SIPTokens = Token.tokenize(SIPLexemes);
    SIPGraph g = Parser.parse(new Definitions.GraphConstructor(), Definitions.GraphParserState.PathOpeningBrace, Definitions.graphParserTransitions, SIPTokens);
    assertNotNull(g.getPassingInformation(new Symbol("jkl"), new Symbol("end")));
  }

  @Test(expected = RuntimeException.class)
  public void test2() {
    StringBuilder input = new StringBuilder("{abc_def, ghi; jkl} -- Foo, Bar --> --> end");
    List<Lexeme<Definitions.Graph>> SIPLexemes = Lexer.lexicalize(Definitions.graphLexers, new StringBuilder(input));
    List<Token<Definitions.Graph>> SIPTokens = Token.tokenize(SIPLexemes);
    Parser.parse(new Definitions.GraphConstructor(), Definitions.GraphParserState.PathOpeningBrace, Definitions.graphParserTransitions, SIPTokens);
  }

  @Test(expected = RuntimeException.class)
  public void test3() {
    StringBuilder input = new StringBuilder("{abc_def, ghi; jkl} -- Foo, Bar -->");
    List<Lexeme<Definitions.Graph>> SIPLexemes = Lexer.lexicalize(Definitions.graphLexers, new StringBuilder(input));
    List<Token<Definitions.Graph>> SIPTokens = Token.tokenize(SIPLexemes);
    Parser.parse(new Definitions.GraphConstructor(), Definitions.GraphParserState.PathOpeningBrace, Definitions.graphParserTransitions, SIPTokens);
  }
}
