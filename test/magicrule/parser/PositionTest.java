package magicrule.parser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PositionTest {
  @Test
  public void test0() {
    assertEquals(5, new Position(5).position);
  }

  @Test
  public void test1() {
    assertEquals("13", new Position(13).toString());
  }
}
