package magicrule.reader;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import magicrule.reader.Partitioner.Part;

public class PartitionerTest {
  final static String nl = System.getProperty("line.separator");

  InputStream input;
  InputStream inputMissingContent;

  String h1 = "Original rule:";
  String h2 = "Modified rule: (bbbb)";
  String c1 = "preceding_sibling(Line1, Line2, Element1, Element2) :-" + nl
              + "  self(Line1, Element1)," + nl
              + "  xml(Line1, Element1, Order1, Level)," + nl
              + "  self(Line2, Element2)," + nl
              + "  xml(Line2, Element2, Order2, Level)," + nl
              + "  lt(Order2, Order1)," + nl
              + "  parent(Line1, Line3, Element1, Element3)," + nl
              + "  parent(Line2, Line3, Element2, Element3).";
  String c2 = "preceding_sibling_h(Line1, Line2, Element1, Element2) :-" + nl
              + "  self.1(Line1, Element1)," + nl
              + "  xml.1(Line1, Element1, Order1, Level)," + nl
              + "  self.2(Line2, Element2)," + nl
              + "  xml.2(Line2, Element2, Order2, Level)," + nl
              + "  lt.1(Order2, Order1)," + nl
              + "  parent.1(Line1, Line3, Element1, Element3)," + nl
              + "  parent.2(Line2, Line3, Element2, Element3).";

  @Before
  public void setUp() {
    String data =
      h1 + nl
      + nl
      + c1 + nl
      + nl
      + nl
      + h2 + nl
      + nl
      + c2 + nl;

    String data2 =
      h1 + nl
      + nl
      + nl
      + nl
      + nl
      + h2 + nl
      + nl
      + c2 + nl;

    input = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
    inputMissingContent = new ByteArrayInputStream(data2.getBytes(StandardCharsets.UTF_8));
  }

  @Test
  public void testPart() {
    Part part = new Part(" header ", " content ");
    assertEquals(" header ", part.header);
    assertEquals(" content ", part.content);
  }

  @Test
  public void testPartition0() {
    List<Part> parts = Partitioner.partition(input);
    assertEquals(2, parts.size());
    assertEquals(h1, parts.get(0).header);
    assertEquals(c1, parts.get(0).content);
    assertEquals(h2, parts.get(1).header);
    assertEquals(c2, parts.get(1).content);
  }

  @Test
  public void testPartition1() {
    List<Part> parts = Partitioner.partition(inputMissingContent);
    assertEquals(2, parts.size());
    assertEquals(h1, parts.get(0).header);
    assertEquals("", parts.get(0).content);
    assertEquals(h2, parts.get(1).header);
    assertEquals(c2, parts.get(1).content);
  }
}
