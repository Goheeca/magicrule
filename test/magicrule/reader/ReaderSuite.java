package magicrule.reader;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  ReaderTest.class,
  PartitionerTest.class
})
public class ReaderSuite {
}
