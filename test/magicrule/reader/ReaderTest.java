package magicrule.reader;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

public class ReaderTest {
  File input;
  File nonAdornedInput;

  @Before
  public void setUp() {
    input = new File("test/magicrule/reader/preceding_sibling_bbbb.txt");
    nonAdornedInput = new File("test/magicrule/reader/preceding_sibling_bbbb-non_adorned.txt");
  }

  @Test
  public void testReadRecord0() {
    Reader.readRecord(input);
  }

  @Test(expected = RuntimeException.class)
  public void testReadRecord1() {
    Reader.readRecord(nonAdornedInput);
  }
}
