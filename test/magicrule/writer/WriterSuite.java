package magicrule.writer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  WriterTest.class,
})
public class WriterSuite {
}
