package magicrule.writer;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import magicrule.PartType;
import magicrule.Record;
import magicrule.reader.Partitioner.Part;

public class WriterTest {
  List<Part> parts;
  String SIP, rule;
  Record record;

  @Before
  public void setUp() {
    String nl = System.getProperty("line.separator");
    parts = new ArrayList<>();
    parts.add(new Part(
                "Original rule:" + nl
                + nl
                + "preceding_sibling(Line1, Line2, Element1, Element2) :-" + nl
                + "  self(Line1, Element1)," + nl
                + "  xml(Line1, Element1, Order1, Level)," + nl
                + "  self(Line2, Element2)," + nl
                + "  xml(Line2, Element2, Order2, Level)," + nl
                + "  lt(Order2, Order1)," + nl
                + "  parent(Line1, Line3, Element1, Element3)," + nl
                + "  parent(Line2, Line3, Element2, Element3)."
              ));
    parts.add(new Part(
                "Modified rule: (bbbb)" + nl
                + nl
                + "preceding_sibling_h(Line1, Line2, Element1, Element2) :-" + nl
                + "  self.1(Line1, Element1)," + nl
                + "  xml.1(Line1, Element1, Order1, Level)," + nl
                + "  self.2(Line2, Element2)," + nl
                + "  xml.2(Line2, Element2, Order2, Level)," + nl
                + "  lt.1(Order2, Order1)," + nl
                + "  parent.1(Line1, Line3, Element1, Element3)," + nl
                + "  parent.2(Line2, Line3, Element2, Element3)."
              ));
    Part partSIP = new Part(
      "SIP:" + nl
      + nl
      + "{preceding_sibling_h} ---------------- Line1, Element1 --> self.1" + nl
      + "{preceding_sibling_h} ---------------- Line1, Element1 --> xml.1" + nl
      + "{preceding_sibling_h} ---------------- Line2, Element2 --> self.2" + nl
      + "{preceding_sibling_h} ---------------- Line2, Element2 --> xml.2" + nl
      + "{preceding_sibling_h} ---------------- Line1, Element1 --> parent.1" + nl
      + "{preceding_sibling_h} ---------------- Line2, Element2 --> parent.2" + nl
      + "{preceding_sibling_h; xml.1} --------- Level ------------> xml.2" + nl
      + "{preceding_sibling_h; xml.1} --------- Order1 -----------> lt.1" + nl
      + "{preceding_sibling_h, xml.1; xml.2} -- Order2 -----------> lt.1" + nl
      + "{preceding_sibling_h; parent.1} ------ Line3, Element3 --> parent.2"
    );
    SIP = partSIP.content;
    parts.add(partSIP);
    parts.add(new Part(
                "FSIP:" + nl
                + nl
                + "{preceding_sibling_h} ------------ Line1, Element1 --> self.1" + nl
                + "{preceding_sibling_h} ------------ Line2, Element2 --> self.2" + nl
                + "{preceding_sibling_h} ------------ Line1, Element1 --> parent.1" + nl
                + "{preceding_sibling_h} ------------ Line2, Element2 --> parent.2" + nl
                + "{preceding_sibling_h; parent.1} -- Line3, Element3 --> parent.2"
              ));
    Part partRule = new Part(
      "Adorned rule:" + nl
      + nl
      + "preceding_sibling_bbbb(Line1, Line2, Element1, Element2) :-" + nl
      + "  self_bb(Line1, Element1)," + nl
      + "  xml(Line1, Element1, Order1, Level)," + nl
      + "  self_bb(Line2, Element2)," + nl
      + "  xml(Line2, Element2, Order2, Level)," + nl
      + "  lt(Order2, Order1)," + nl
      + "  parent_bfbf(Line1, Line3, Element1, Element3)," + nl
      + "  parent_bbbb(Line2, Line3, Element2, Element3)."
    );
    rule = partRule.content;
    parts.add(partRule);
    record = new Record(parts, SIP, rule);
    record.generateMagicForm();
  }

  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  @Test
  public void testWriteRecord0() throws IOException {
    final File expected = new File("test/magicrule/writer/etalon.txt");
    final File output = folder.newFile("tmp");
    Writer.writeRecord(output, record);
    assertArrayEquals(Files.readAllLines(expected.toPath()).toArray(), Files.readAllLines(output.toPath()).toArray());
  }

  @Test
  public void testWriteRecord1() throws IOException {
    @SuppressWarnings("serial")
    Record emptyRecord = new Record(new ArrayList<Part>() {
      {
        Part SIP = new Part("SIP:", "");
        SIP.type = Optional.of(PartType.SIP);
        add(SIP);
        Part adorned = new Part("Adorned rule:", "");
        adorned.type = Optional.of(PartType.ADORNED_RULE);
        add(adorned);
        Part magic = new Part("Magic rules:", "");
        magic.type = Optional.of(PartType.MAGIC_RULES);
        add(magic);
      }
    }, "", "a(X):-b(X).");
    final File expected = new File("test/magicrule/writer/etalon-empty.txt");
    final File output = folder.newFile("tmp");
    Writer.writeRecord(output, emptyRecord);
    assertArrayEquals(Files.readAllLines(expected.toPath()).toArray(), Files.readAllLines(output.toPath()).toArray());
  }
}
